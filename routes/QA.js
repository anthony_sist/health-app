exports.register = function(server, options, next) {
    var io = require('socket.io').listen(server.select('feed').listener, {
        log: false
    });
    'use strict';
    var watson = require('watson-developer-cloud');
    var watsonAns;
    var question_and_answer_healthcare = watson.question_and_answer({
        username: "84fb463c-0584-4ac0-9164-8c2eefe24b68",
        password: "tJfJGpWKIzg7",
        version: 'v1',
        dataset: 'healthcare'
        /* The dataset can be specified when creating
         * the service or when calling it */
    });
    io.sockets.on('connection', function(socket) {
        socket.on('search', function(term) {
            question_and_answer_healthcare.ask({
                text: term,
				formattedAnswer: true
            }, function(err, response) {
                if(err) console.log('error:', err);
                else watsonAns = response
				socket.emit('watsonAns', watsonAns)
            });
        })
        
    });
};
exports.register.attributes = {
    name: 'socket plugin'
};