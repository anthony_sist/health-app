var Hapi = require('hapi');
//get server object
var Server = new Hapi.Server();
var Path = require('path');

Server.connection({
    port: 3000
});
//configure view redering
Server.views({
    engines: {
        html: require('handlebars')
    },
    path: Path.join(__dirname, 'public')
});
Server.route({
    method: 'GET',
    path: '/',
    handler: function(req, reply) {
        reply.view('index');
    }
})
Server.route({
    method: 'GET',
    path: '/{param*}',
    handler: {
        directory: {
            path: 'public'
        }
    }
});
//configure websockets
Server.connection({
    port: 8080,
    labels: 'feed'
});
Server.register({
    register: require('./routes/QA')
}, function(err) {
    if(err) throw err;
});

Server.start();