(function() {
    angular.module('newApp').controller('homeCtrl', homeCtrl)
    homeCtrl.$inject = ['$scope'];

    function homeCtrl($scope) {
        //Assign viewmodel
        var vm = this;
        vm.doSearch = doSearch;

        function doSearch() {
            return socket.emit('search', vm.searchTerm);
        }
        //get comment feed socket
        var socket = io(':8080');
        socket.on('watsonAns', function(data) {
            console.log(data);
            vm.watsonAnswer = data;
            vm.confidence = getConfidence();
            $scope.$apply();
        });
        var getConfidence = function() {
            var level = vm.watsonAnswer[0].question.answers[0].confidence;
            if(level < .6) {
                if(level < .3) {
                    return "poor";
                } else {
                    return "ok";
                }
            } else {
                return "good";
            }
        }
        var searchFocus = function() {
            document.getElementById("searchBar").focus();
        }
        var commands = {
            'search': doSearch
        };
        var focusSearch = {
            'Do a search': searchFocus
        }
        annyang.addCallback('resultNoMatch', function(userSaid, commandText, phrases) {
            if(document.getElementById("searchBar") == document.activeElement) {
                vm.searchTerm = userSaid[0];
                $scope.$apply();
            }
        });
        // initialize annyang, overwriting any previously added commands
        annyang.addCommands(commands);
        annyang.addCommands(focusSearch);
        annyang.start();
    }
})();